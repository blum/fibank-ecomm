# FIBANK ECOMM Virtual POS terminal module for PHP 
## Laravel 7 Support (Should work on 5.* and 6.* as well)

## How to use
Install the pack with composer
```
$ php composer require i-creativ/fibank-ecomm
```
## 1. Use in a (vanilla) PHP project
### Init and config:
```php
<?php
require __DIR__ . '/vendor/autoload.php';

// set the config

$_ENV['fibank_ecomm.config.ecomm_env'] = 'test'; 
// if 'test' it uses the URLs for the test VPOS 
// set to 'production' to use the real production VPOS 

$_ENV['fibank_ecomm.config.ecomm_cert_location'] = "your_certificate.pem";
$_ENV['fibank_ecomm.config.ecomm_cert_password'] = "<your-certificate-password>";

```

### Creating transaction:
```php
<?php

$fibankEcomm = new \ICreativ\FibankEcomm\FibankEcomm();

$fibankEcomm->create('<order-number>', '<amount-bgn>', '<ip-of-the-client>');
// amount format: 2.30

```

Getting the transaction URL:

```php
<?php

$url = $fibankEcomm
    ->getClientRedirectionURL()
;
// redirect the client to $url to make the payment on the terminal


//Getting the transaction ID:
$transaction_id = $fibankEcomm->getTransactionId();
```

### Quering ECOMM about a transaction

```php
<?php
$fibankEcomm = new \ICreativ\FibankEcomm\FibankEcomm();

$fibankEcomm
    ->extractTransactionResult('<transaction-id>', '<ip-of-the-client>*');
;
// * The IP that initiated the transaction

// Shows if the transaction was successful
$fibankEcomm->isSuccessful(); // true/false

// Get the result details 
$result = $fibankEcomm->getTransactionResult(); 
// The response by ECOMM, parsed in array
```

### Refund a transaction

```php
<?php
$fibankEcomm = new \ICreativ\FibankEcomm\FibankEcomm();

$fibankEcomm
    ->refundTransaction('<transaction-id>', '<amount>')
;
// amount format: 2.10

// Shows if the refund was successful
$fibankEcomm->isSuccessful(); // true/false

// The response by ECOMM, parsed in array
$result = $fibankEcomm->getTransactionRefundResult();

// The refund transaction ID
$refund_trans_id = $fibEcomm->getTransactionRefundId();

```


### Reverse a transaction

```php
<?php
$fibankEcomm = new \ICreativ\FibankEcomm\FibankEcomm();

$fibankEcomm
    ->reverseTransaction('<transaction-id>')
;

// Shows if the reverse was successful
$fibankEcomm->isSuccessful(); // true/false

// The response by ECOMM, parsed in array
$result = $fibankEcomm->getTransactionReverseResult();
```

## 2. Use in a Laravel project
The service provider should be auto discovered on composer install.

Publish the configs:
```
$ php artisan vendor:publish --provider="ICreativ\FibankEcomm\FibankEcommServiceProvider" --tag="config" 
```
and set the options in /config/fibank_ecomm.config.php (Location of your .pem certificate file and its password).

After that just use FibankEcomm object as shown above, e.g.:

```php
<?php

use ICreativ\FibankEcomm\FibankEcomm;

class SomeControler (FibankEcomm $fibankEcomm) {
    
    $fibankEcomm->create('123-ABC', 2.34, '11.22.33.44');
    // ...
}
```

<?php

namespace ICreativ\FibankEcomm;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class FibankEcomm
{
    private $api_url;
    private $payment_page_url;
    private $transaction_id;
    private $transaction_refund_id;
    private $transaction_result;
    private $transaction_refund_result;
    private $transaction_reverse_result;
    private $success_state;
    private $pending_state;

    protected $httpClient;

    public function __construct($httpClient = null)
    {
        // set the config
        foreach ([
            'ecomm_env' => 'test',
            "ecomm_description_suffix" => '',
            'ecomm_api_url_production' => 'https://mdpay.fibank.bg:10443/ecomm_v2/MerchantHandler',
            'ecomm_payment_page_url_production' => 'https://mdpay.fibank.bg/ecomm_v2/ClientHandler',
            'ecomm_api_url_test' => 'https://mdpay-test.fibank.bg:10443/ecomm_v2/MerchantHandler',
            'ecomm_payment_page_url_test' => 'https://mdpay-test.fibank.bg/ecomm_v2/ClientHandler',
        ] as $k => $v) {
            if (!isset($_ENV['fibank_ecomm.config.' . $k])) {
                $_ENV['fibank_ecomm.config.' . $k] = $v;
            }
        }

        if ($_ENV['fibank_ecomm.config.ecomm_env'] == 'production') {
            $this->api_url = $_ENV['fibank_ecomm.config.ecomm_api_url_production'];
            $this->payment_page_url = $_ENV['fibank_ecomm.config.ecomm_payment_page_url_production'];
        } else {
            $this->api_url = $_ENV['fibank_ecomm.config.ecomm_api_url_test'];
            $this->payment_page_url = $_ENV['fibank_ecomm.config.ecomm_payment_page_url_test'];
        }

        $this->httpClient = $httpClient ?? new Client();
    }

    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    public function getTransactionRefundResult()
    {
        return $this->transaction_refund_result;
    }

    public function getTransactionReverseResult()
    {
        return $this->transaction_reverse_result;
    }

    public function getTransactionRefundId()
    {
        return $this->transaction_refund_id;
    }

    public function getTransactionResult()
    {
        return $this->transaction_result;
    }

    /**
     * Creates transaction
     *
     * @param string $order_id
     * @param float $amount
     * @param string $ip
     * @param string $currency
     *
     * @return FibankEcomm
     */
    public function create($order_id, $amount, $ip, $currency = "BGN", $language = "default") : FibankEcomm
    {
        $amount = (int) ((float) $amount * 100);
        if ($currency == 'BGN') {
            $currency = 975;
        }

        $prm_data = [
            'command' => 'v',
            'amount' => $amount,
            'currency' => $currency,
            'client_ip_addr' => $ip,
            'description' => $order_id . $_ENV['fibank_ecomm.config.ecomm_description_suffix'],
            'language' => $language
        ];


        $result = $this->request($prm_data);
        if (\strstr($result, "TRANSACTION_ID")) {
            $this->transaction_id = $this->parseResponse($result)['TRANSACTION_ID'];
        } else {
            throw new \Exception('Error on FIB request:' . $result, 1);
        }

        return $this;
    }

    /**
     * Parses response into an array
     *
     * @param string $result
     *
     * @return array
     */
    public function parseResponse($result): array
    {
        $data = [];
        foreach (\explode(PHP_EOL, $result) as $line) {
            if (!strstr($line, ':')) {
                continue;
            }
            $couple = \explode(':', $line);
            $data[trim($couple[0])] = trim($couple[1]);
        }

        return $data;
    }

    /**
     * Provides the URL to redirect to
     *
     * @return string
     */
    public function getClientRedirectionURL(): string
    {
        return $this->payment_page_url . '?trans_id=' . urlencode($this->transaction_id);
    }

    /**
     * Undocumented function
     *
     * @param string $trans_id
     * @param string $ip
     *
     * @return FibankEcomm
     */
    public function extractTransactionResult($trans_id, $ip) : FibankEcomm
    {
        $prm_data = [
            'command' => 'c',
            'trans_id' => $trans_id,
            'client_ip_addr' => $ip,
        ];

        $result = $this->request($prm_data);

        if (substr($result, 0, 5) != 'error') {
            $result = $this->transaction_result = $this->parseResponse($result);
        } else {
            throw new \Exception('Error on FIB transaction result:' . $result, 1);
        }

        if ($result['RESULT'] == 'OK') {
            $this->success_state = true;
            $this->pending_state = false;
        } elseif (in_array($result['RESULT'], ['FAILED', 'DECLINED', 'AUTOREVERSED', 'TIMEOUT'])) {
            $this->pending_state = false;
            $this->success_state = false;
        } elseif (in_array($result['RESULT'], ['CREATED', 'PENDING'])) {
            $this->pending_state = true;
            $this->success_state = null;
        }

        return $this;
    }

    /**
     * Refunds a transaction
     *
     * @param string $trans_id
     * @param float $amount
     *
     * @return FibankEcomm
     */
    public function refundTransaction($trans_id, $amount) : FibankEcomm
    {
        $amount = (int) ((float) $amount * 100);

        $prm_data = [
            'command' => 'k',
            'trans_id' => $trans_id,
            'amount' => $amount,
        ];

        $result = $this->request($prm_data);

        if (substr($result, 0, 5) != 'error') {
            $result = $this->transaction_refund_result = $this->parseResponse($result);
            $this->transaction_refund_id = $result['REFUND_TRANS_ID'];
        } else {
            throw new \Exception('Error on FIB refund result:' . $result, 1);
        }

        if ($result['RESULT'] == 'OK') {
            $this->success_state = true;
        } elseif (in_array($result['RESULT'], ['FAILED'])) {
            $this->success_state = false;
        }

        return $this;
    }

    /**
     * Reverses a transaction
     *
     * @param string $trans_id
     *
     * @return FibankEcomm
     */
    public function reverseTransaction($trans_id) : FibankEcomm
    {
        $prm_data = [
            'command' => 'r',
            'trans_id' => $trans_id
        ];

        $result = $this->request($prm_data);

        if (substr($result, 0, 5) != 'error') {
            $result = $this->transaction_reverse_result = $this->parseResponse($result);
        } else {
            throw new \Exception('Error on FIB reverse result:' . $result, 1);
        }

        if ($result['RESULT'] == 'OK') {
            $this->success_state = true;
        } elseif (in_array($result['RESULT'], ['FAILED', 'REVERSED'])) {
            $this->success_state = false;
        }

        return $this;
    }

    /**
     * Indicates success
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->success_state === true;
    }

    /**
     * Indicate error
     *
     * @return boolean
     */
    public function isUnsuccessful(): bool
    {
        return $this->success_state === false;
    }

    public function isPending(): bool
    {
        return $this->pending_state === true;
    }

    /**
     * Undocumented function
     *
     * @param array $prm_data
     *
     * @return string
     */
    private function request($prm_data) : string
    {
        $client = $this->httpClient;

        try {
            $response = $client->request(
                'POST',
                $this->api_url,
                [
                    'form_params' => $prm_data,

                    'verify' => false,
                    'cert' => [$_ENV['fibank_ecomm.config.ecomm_cert_location'], $_ENV['fibank_ecomm.config.ecomm_cert_password']],
                ]
            );
        } catch (ClientException $e) {
            throw $e;
        }

        return $response->getBody();
    }
}

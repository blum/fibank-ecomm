<?php

return [
    'ecomm_env' => env('ECOMM_ENV', 'test'),
    'ecomm_description_suffix' => env('ECOMM_DESCRIPTION_SUFFUX', ''),
    'ecomm_cert_location' => env('ECOMM_CERT_LOCATION'),
    'ecomm_cert_password' => env('ECOMM_CERT_PASSWORD'),

    'ecomm_api_url_production' => 'https://mdpay.fibank.bg:10443/ecomm_v2/MerchantHandler',
    'ecomm_payment_page_url_production' => 'https://mdpay.fibank.bg/ecomm_v2/ClientHandler',

    'ecomm_api_url_test' => 'https://mdpay-test.fibank.bg:10443/ecomm_v2/MerchantHandler',
    'ecomm_payment_page_url_test' => 'https://mdpay-test.fibank.bg/ecomm_v2/ClientHandler',
];

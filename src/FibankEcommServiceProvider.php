<?php

namespace ICreativ\FibankEcomm;

use Illuminate\Support\ServiceProvider;

class FibankEcommServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Publish the config
        $this->publishes([
            __DIR__ . '/config/fibank_ecomm.config.php' => config_path('fibank_ecomm.config.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/fibank_ecomm.config.php',
            'fibank_ecomm.config'
        );

        foreach ([
            "ecomm_env",
            "ecomm_api_url_production",
            'ecomm_description_suffix',
            "ecomm_payment_page_url_production",
            "ecomm_api_url_test",
            "ecomm_payment_page_url_test",
            "ecomm_cert_location",
            "ecomm_cert_password",
        ] as $v) {
            $_ENV['fibank_ecomm.config.' . $v] = config('fibank_ecomm.config.' . $v);
        }

        $this->app->singleton(FibankEcomm::class, function () {
            return new FibankEcomm();
        });
    }
}

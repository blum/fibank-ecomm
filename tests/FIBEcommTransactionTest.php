<?php

namespace ICreativ\FibankEcomm\Test;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use ICreativ\FibankEcomm\FibankEcomm;

class FIBEcommTransactionTest extends TestCase
{
    protected $mockHandler;
    protected $fibankEcomm;

    public function setUp():void
    {
        parent::setUp();

        $this->ip = '11.11.11.11';

        $this->mockHandler = new MockHandler();

        $httpClient = new Client([
            'handler' => $this->mockHandler,
        ]);

        $this->fibankEcomm = new FibankEcomm($httpClient);
    }

    public function testCreateTransaction()
    {
        $preset_transaction_id = '===transaction-id===';

        $this->mockHandler->append(new Response(200, [], "TRANSACTION_ID: {$preset_transaction_id}"));

        $fibEcomm = $this->fibankEcomm->create('004-AAA', 1.50, $this->ip);

        $transaction_id = $fibEcomm->getTransactionId();
        $this->assertSame($preset_transaction_id, $transaction_id);

        $url = $fibEcomm
            ->getClientRedirectionURL()
        ;

        $this->assertSame($_ENV['fibank_ecomm.config.ecomm_payment_page_url_test'] . '?trans_id=' . urlencode($preset_transaction_id), $url, 'URL incorrect');
    }

    public function testExtractTransactionResultOK()
    {
        $preset_transaction_id = '===transaction-id-success===';

        $this->mockHandler->append(new Response(200, [], '
            RESULT: OK
            RESULT_PS: FINISHED
            RESULT_CODE: 000
            3DSECURE: OK
            RRN: 123456789012
            APPROVAL_CODE: 123456
            CARD_NUMBER: 4***********6789
            '));

        $fibEcomm = $this->fibankEcomm;

        $fibEcomm
            ->extractTransactionResult($preset_transaction_id, $this->ip)
        ;

        $this->assertTrue($fibEcomm->isSuccessful());

        $this->assertSame($fibEcomm->getTransactionResult()['APPROVAL_CODE'], '123456');
    }

    public function testExtractTransactionResultUnsuccessful()
    {
        $preset_transaction_id = '===transaction-id-error===';

        $this->mockHandler->append(new Response(200, [], '
            RESULT: FAILED  
            RESULT_CODE: 129
            3DSECURE: NOTPARTICIPATED
            RRN: 013708715896
            APPROVAL_CODE: 201433
            CARD_NUMBER: 5***********4936
            '));

        $fibEcomm = $this->fibankEcomm;

        $fibEcomm
            ->extractTransactionResult($preset_transaction_id, $this->ip)
        ;

        $this->assertTrue($fibEcomm->isUnsuccessful());
        $this->assertFalse($fibEcomm->isSuccessful());
        $this->assertFalse($fibEcomm->isPending());

        $this->assertSame($fibEcomm->getTransactionResult()['RESULT_CODE'], '129');
    }

    public function testExtractTransactionResultPending()
    {
        $preset_transaction_id = '===transaction-id-pending===';

        $this->mockHandler->append(new Response(200, [], '
                RESULT: CREATED
                3DSECURE: FAILED
            '));

        $fibEcomm = $this->fibankEcomm;

        $fibEcomm
            ->extractTransactionResult($preset_transaction_id, $this->ip)
        ;

        $this->assertTrue($fibEcomm->isPending());
        $this->assertFalse($fibEcomm->isSuccessful());
    }

    public function testRefundTransactionSuccess()
    {
        $preset_transaction_id = '===transaction-id-refund===';

        $this->mockHandler->append(new Response(200, [], '
            RESULT: OK
            RESULT_CODE: 000
            REFUND_TRANS_ID: eqdrGwr30JFdrMp7UUJDzo+0ui8=
            '));

        $fibEcomm = $this->fibankEcomm;

        $fibEcomm
            ->refundTransaction($preset_transaction_id, 1.20)
        ;

        $this->assertTrue($fibEcomm->isSuccessful());

        $this->assertSame($fibEcomm->getTransactionRefundResult()['RESULT_CODE'], '000');
        $this->assertSame($fibEcomm->getTransactionRefundId(), 'eqdrGwr30JFdrMp7UUJDzo+0ui8=');
    }

    public function testReverseTransactionSuccess()
    {
        $preset_transaction_id = '===transaction-id-reverse===';

        $this->mockHandler->append(new Response(200, [], '
            RESULT: OK
            RESULT_CODE: 400
        '));

        $fibEcomm = $this->fibankEcomm;

        $fibEcomm
            ->reverseTransaction($preset_transaction_id, 1.20)
        ;

        $this->assertTrue($fibEcomm->isSuccessful());

        $this->assertSame($fibEcomm->getTransactionReverseResult()['RESULT_CODE'], '400');
    }
}

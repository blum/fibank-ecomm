<?php

namespace ICreativ\FibankEcomm\Test;

use PHPUnit\Framework\TestCase as PHPUnitTestCase;

class TestCase extends PHPUnitTestCase
{
    public function setUp():void
    {
        parent::setUp();

        $_ENV['fibank_ecomm.config.ecomm_env'] = 'test';
        $_ENV['fibank_ecomm.config.ecomm_api_url_production'] = 'https://mdpay.fibank.bg:10443/ecomm_v2/MerchantHandler';
        $_ENV['fibank_ecomm.config.ecomm_payment_page_url_production'] = 'https://mdpay.fibank.bg/ecomm_v2/ClientHandler';
        $_ENV['fibank_ecomm.config.ecomm_api_url_test'] = 'https://mdpay-test.fibank.bg:10443/ecomm_v2/MerchantHandler';
        $_ENV['fibank_ecomm.config.ecomm_payment_page_url_test'] = 'https://mdpay-test.fibank.bg/ecomm_v2/ClientHandler';

        $_ENV['fibank_ecomm.config.ecomm_cert_location'] = __DIR__ . '/provision/test_cert_fake.pem';
        $_ENV['fibank_ecomm.config.ecomm_cert_password'] = 'password';
    }
}
